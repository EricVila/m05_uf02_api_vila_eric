<?php

error_reporting(0);
require(__DIR__.'/lib/HTTPClient.php');
require(__DIR__.'/lib/JSONParser.php');

# Primer instanciar la classe, amb la base URL on hi ha la nostre api
$base_url   = 'http://127.0.0.1:3000/';
$token      = 'C0UsWlYxXrMx81TKN2Eq';
$client     = new HTTPClient($base_url, $token);

$editar_id = $_GET["editar_id"];
$tipus = $_GET["tipus"];
if($tipus == "assignatures"){
  if(isset($editar_id))
  {
    try
    {
      $result = $client->query('/api/v1/assignatura/'.$editar_id);
      $nom = $resultat["data"]->nom;
      $professor = $result["data"]->professor;
    }
    catch (\Exception $e)
    {
      header("Location:app.php");
    }
  }
?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8">
    <title>Pràctica Pelicules</title>
    <link rel="stylesheet" href="pelisMySQL.css">
  </head>
  <body>
    <form class="formulari" method="POST">
      <p><h2><u>Gestió de pelicules :</u></h2></p>
      <p><h4>Nom : <input type="text" name="nom" value="<?php echo $nom ?>" align="right" placeholder="Títol de la pelicula" required/></h4></p>
      <p><h4>Professor : <input type="text" name="professor" value="<?php echo $director ?>" placeholder="Director de la pelicula" required/></h4></p>
      <p><input type="submit" value="Guardar" name="Boto"/></p>
    </form>
    </body>
  </body>
</html>

<?php
if(isset($_POST['Boto']))
{
  $nom = $_POST['nom'];
  $professor = $_POST['professor'];
  $params = [
    'nom' => $nom,
    'professor' => $professor
  ];
  $result = $client->query('/api/v1/assignatura/'.$editar_id, $params, 'PUT');
  header("location:llista.php?tipus=assignatures");
}
}
else if($tipus == "alumnes"){
  if(isset($editar_id))
  {
    try
    {
      $result = $client->query('/api/v1/alumne/'.$editar_id);
      $nom = $result["data"]->nom;
      $cognoms = $result["data"]->cognoms;
      $mail = $result["data"]->mail;
    }
    catch (\Exception $e)
    {
      header("Location:app.php");
    }
  }
?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8">
    <title>Pràctica Pelicules</title>
    <link rel="stylesheet" href="pelisMySQL.css">
  </head>
  <body>
    <form class="formulari" method="POST">
      <p><h2><u>Gestió de pelicules :</u></h2></p>
      <p><h4>Nom : <input type="text" name="nom" value="<?php echo $nom ?>" align="right" required/></h4></p>
      <p><h4>Cognoms : <input type="text" name="cognom" value="<?php echo $cognoms ?>" required/></h4></p>
      <p><h4>Correu electrònic : <input type="text" name="mail" value="<?php echo $mail ?>" required/></h4></p>
      <p><input type="submit" value="Guardar" name="Boto"/></p>
    </form>
    </body>
  </body>
</html>
<?php
if(isset($_POST['Boto']))
{
  $nom = $_POST['nom'];
  $cognom = $_POST['cognom'];
  $mail = $_POST['mail'];
  $params = [
    'nom' => $nom,
    'cognoms' => $cognom,
    'mail' => $mail
  ];
  $result = $client->query('/api/v1/alumne/'.$editar_id, $params, 'PUT');
  header("location:llista.php?tipus=alumnes");

}
  }
 ?>
