<?php

error_reporting(0);
require(__DIR__.'/lib/HTTPClient.php');
require(__DIR__.'/lib/JSONParser.php');

# Primer instanciar la classe, amb la base URL on hi ha la nostre api
$base_url   = 'http://127.0.0.1:3000/';
$token      = 'C0UsWlYxXrMx81TKN2Eq';
$client     = new HTTPClient($base_url, $token);

$tipus = $_GET["tipus"];
if($tipus == "alumnes"){

?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8">
    <title>Llistat</title>
    <style>
table
{
border-collapse: collapse;
width: 100%;
background-color: grey;
}

.fila
{
background-color: black;
color: white;
}

th, td
{
text-align: center;
padding: 8px;
}

tr:nth-child(even) { background-color: #f2f2f2; }
    </style>
  </head>
  <body style="background:#045FB4;">
    <form method="POST">
    <br>
    <p><h1 align="center"><u>Vista d'alumnes</u></h1></p>
      <table border="5">
        <tr class="fila">
          <td>ID</td>
          <td>Nom</td>
          <td>Cognoms</td>
          <td>Correu electrònic</td>
          <td colspan="2">Opcions</td>
        </tr>
        <?php
        $count = 0;
        $result = $client->query('/api/v1/alumnes');
        while($result["data"][$count] != null){
        ?>
        <tr>
          <td><?php echo $result["data"][$count]->id ?></td>
          <td><?php echo $result["data"][$count]->nom ?></td>
          <td><?php echo $result["data"][$count]->cognoms ?></td>
          <td><?php echo $result["data"][$count]->mail ?></td>
          <td><input type="submit" name="editar" value="<?php echo 'EDITAR '.$result["data"][$count]->id ?>" style="width:58px;"/></td>
          <td><input type="submit" value="<?php echo 'ESBORRAR '.$result["data"][$count]->id ?>" style="width:85px;" name="esborrar"/></td>
        </tr>
        <?php
        $count++;
          }
          if(isset($_POST['esborrar']))
          {
            $id = $_POST['esborrar'];
            $esborrar_id = str_replace("ESBORRAR ", "", $id);
            $result = $client->query('/api/v1/alumne/'.$esborrar_id, [], 'DELETE');
            echo '<meta http-equiv="refresh" content="0">';
          }
          else if (isset($_POST['editar']))
          {
            $id = $_POST['editar'];
            $editar_id = str_replace("EDITAR ", "", $id);
            header("location:editar.php?tipus=alumnes&editar_id=".$editar_id);
          }
        ?>
      </table>
    </form>
  </body>
</html>

<?php
}
elseif ($tipus == "assignatures") {

?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8">
    <title>Llistat</title>
    <style>
table
{
border-collapse: collapse;
width: 100%;
background-color: grey;
}

.fila
{
background-color: black;
color: white;
}

th, td
{
text-align: center;
padding: 8px;
}

tr:nth-child(even) { background-color: #f2f2f2; }
    </style>
  </head>
  <body style="background:#045FB4;">
    <form method="POST">
    <br>
    <p><h1 align="center"><u>Vista d'assignatures</u></h1></p>
      <table border="5">
        <tr class="fila">
          <td>ID</td>
          <td>Nom</td>
          <td>Professor</td>
          <td colspan="2">Opcions</td>
        </tr>
        <?php
        $count = 0;
        $result = $client->query('/api/v1/assignatures');
        while($result["data"][$count] != null){
        ?>
        <tr>
          <td><?php echo $result["data"][$count]->id ?></td>
          <td><?php echo $result["data"][$count]->nom ?></td>
          <td><?php echo $result["data"][$count]->professor ?></td>
          <td><input type="submit" name="editar" value="<?php echo 'EDITAR '.$result["data"][$count]->id ?>" style="width:58px;"/></td>
          <td><input type="submit" value="<?php echo 'ESBORRAR '.$result["data"][$count]->id ?>" style="width:85px;" name="esborrar"/></td>
        </tr>
        <?php
        $count++;
          }
          if(isset($_POST['esborrar']))
          {
            $id = $_POST['esborrar'];
            $esborrar_id = str_replace("ESBORRAR ", "", $id);
            $result = $client->query('/api/v1/assignatura/'.$esborrar_id, [], 'DELETE');
            echo '<meta http-equiv="refresh" content="0">';
          }
          else if (isset($_POST['editar']))
          {
            $id = $_POST['editar'];
            $editar_id = str_replace("EDITAR ", "", $id);
            header("location:editar.php?tipus=assignatures&editar_id=".$editar_id);
          }
        ?>
      </table>
    </form>
  </body>
</html>

<?php
}
?>
