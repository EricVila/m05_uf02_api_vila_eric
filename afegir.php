<?php

error_reporting(0);
require(__DIR__.'/lib/HTTPClient.php');
require(__DIR__.'/lib/JSONParser.php');

# Primer instanciar la classe, amb la base URL on hi ha la nostre api
$base_url   = 'http://127.0.0.1:3000/';
$token      = 'C0UsWlYxXrMx81TKN2Eq';
$client     = new HTTPClient($base_url, $token);

$tipus = $_GET["tipus"];
if($tipus == "assignatures"){

?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8">
    <title>Aplicació</title>
  </head>
  <body>
    <form method="POST" class="formulari">
      <p><h1 align="center"><u>Afegir Assignatura</u></h1></p>
      <p><h4>Nom : <input type="text" name="nom" align="right"/></h4></p>
      <p><h4>Professor : <input type="text" name="professor"/></h4></p>
      <p><input type="submit" value="Afegir" name="afegir"/></p><br/>
    </form>
    </body>
  </body>
</html>

<?php
$nom = $_POST["nom"];
$professor = $_POST["professor"];
if($nom != "" && $professor != ""){
  $params = [
    'nom' => $nom,
    'professor' => $professor
  ];
  $result = $client->query('/api/v1/assignatura', $params, 'POST');
  header("location:app.php");
}

?>

<?php

}
else if($tipus == "alumnes"){

 ?>

 <!DOCTYPE html>
 <html lang="es">
   <head>
     <meta charset="UTF-8">
     <title>Aplicació</title>
   </head>
   <body>
     <form method="POST" class="formulari">
       <p><h1 align="center"><u>Afegir Alumne</u></h1></p>
       <p><h4>Nom : <input type="text" name="nom" align="right"/></h4></p>
       <p><h4>Cognoms : <input type="text" name="cognoms"/></h4></p>
       <p><h4>Correu Electrònic : <input type="text" name="email"/></h4></p>
       <p><input type="submit" value="Afegir" name="afegir"/></p><br/>
     </form>
     </body>
   </body>
 </html>

<?php
$nom = $_POST["nom"];
$cognom = $_POST["cognoms"];
$mail = $_POST["email"];
if($nom != "" && $cognom != "" && $mail != ""){
  $params = [
    'nom' => $nom,
    'cognoms' => $cognom,
    'mail' => $mail
  ];
  $result = $client->query('/api/v1/alumne', $params, 'POST');
  header("location:app.php");
}


?>

 <?php

}

?>
